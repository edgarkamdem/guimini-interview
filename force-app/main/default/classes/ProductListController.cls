public class ProductListController {
     @AuraEnabled
     public static List < Product2 > fetchProduct2(String searchKeyWord) {
      String searchKey = '%'+searchKeyWord + '%';
      return [SELECT Name, Family, Description 
                                           FROM Product2
                                       	  WHERE Name LIKE : searchKey LIMIT 500];
     
     }
    
}