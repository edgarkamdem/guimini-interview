@isTest
public class Product2TriggerTest {
    @isTest
    private static void testMethodOne() {
        insertData();
 		User currentUser = [SELECT Id FROM User WHERE LastName = 'Guimini' LIMIT 1];
        System.runAs(currentUser) {
            ProductListController.fetchProduct2('Test');
        }
    }
    
    @isTest
    private static void insertData() {
        Contact myTestContact = new Contact(
            FirstName = 'Guimini',
            LastName = 'Guimini',
            Email = 'Guimini.Guimini@test.com'
        );
        insert myTestContact;
        
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name LIKE '%dmin%'];

        User myUserTest = new User(
            Alias = 'Guimini',
            Email = 'Guimini.Guimini@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Guimini',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = 'Guimini.Guimini@test.com'
        );
        insert myUserTest;
        Product2 pd2 = new Product2(Name = 'TestTest');
        insert pd2;
        update pd2;
        update pd2;
    }
}