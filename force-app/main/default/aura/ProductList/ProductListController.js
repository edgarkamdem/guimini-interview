({
    Search: function(component, event, helper) {
        let searchField = component.find('searchField');
        let isValueMissing = searchField.get('v.validity').valueMissing;
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            helper.SearchHelper(component, event);
        }
    },
    updateProductDetailFields: function(component, event, helper){
        
        component.get('v.searchResult').forEach(function(item){
            console.log('item', item.Id , event.currentTarget.id);
            if(item.Id ==  event.currentTarget.id){
                component.set('v.parentProductName', item.Name);
                component.set('v.parentProductFamily', item.Family);
                component.set('v.parentProductDescription', item.Description);
            } 
            console.log('parentProductName', component.get('v.parentProductDescription'));
        });
    }
})