I. Questions générales

1. Quel est le cycle de vie d’un record dans Salesforce ?  
Cette question est un peu floue pour moi, désolé! Sinon de ma compréhension, je pense qu'un record suis le cycle suivant:
Il est créé, puis peut être lu et modifié, et enfin peut être supprimé.
2. Quels sont les éléments à prendre à compte lorsqu’on crée un process builder ? 
  - L'objet sur lequel on souhaite déclencher le process builder
  - Le(s) critère(s) de déclenchement du process builder
  - Le(s) actions(s) qu'on souhaite éxécuter lors  de(s) déclenchement(s) du/des critère(s)
3. ChangeSet ou SFDX ? Pourquoi ?  
Les 2 ont chacun leur rôle. Les changesets nous permettent facilement de mettre sur pieds des paquets de déploiement de certains
éléments précis qu'on peut après déployer vers des sandbox/orgs en utilisant plusieurs outils comme ANT entre autre. 
D'un autre côté SFDX permet principalement de mettre en place des scratch orgs et d'effectuer du développement continu à l'aide de ligne de commande.
Cependant la limite de 30 jours maximum pour le maintien des scratch par Salesforce, l'impossibilité de déployer qu'un morceau précis de notre projet,
met en avant ses limites qui sont pourtant bien gérées par le système des changesets.

II. Exercices pratiques

1. Créer un composant Lightning ProductList  
  - Qui affiche la liste des produits (Product2)  
  - L’utilisateur doit être capable de rechercher un produit par son nom  
  - L’utilisateur ne doit avoir accès qu’aux produits dont il a droit.  
2. Créer un composant Lightning ProductDetail  
- Qui affiche les détails d’un produit lorsqu’on clique sur un produit dans la liste des produits.

3. Créer un champ sur Product2 qui compte le nombre de fois qu’un produit a été modifié

4. Crée un process builder qui envoie un mail au propriétaire du produit lorsque le nombre de modifications atteint 10, en mettant en copie une adresse email que tu définiras.

5. Créer une application (Guimini Products) Lightning qui rassemble la liste des produits et les détails d’Un produit en bas de la liste.

6. Rassemble l’ensemble des composants et envois sur un dépôt git (gitlab) au format sfdx.

7. Utilise les bonnes pratiques de programmation des Triggers et des Classes Apex.

Bon courage !